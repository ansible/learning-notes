* the most common file formats are YAML and INI (IMO latter is better for nested ones)
* FQDN or IP address is enough to define the host
* the host grouping feature:
  - good practice is to put each host in more than one group (categorization)

    for example, you can create groups tracking:
    * what: an application, stack or micro-service is deployed (i.e. database or
      web server)
    * where: a data-center or region
    * when: a development stage (dev, staging, prod)
* there are two default host groups:
  - **all** (each defined host)
  - **ungrouped** (ones without any group)

* features:
  - adding ranges of hosts:
    * there is no need to create tons of lines when adding hosts to the groups:

    ```
    [HOST_GROUP]
    www[01:50].example.com
    ```

    * also we can specify iteration step (stride, increments between sequence
    numbers):

    ```
    [HOST_GROUP]
    www[01:50:2].example.com
    ```

    * also it is possible to define alphabetic ranges:

    ```
    [HOST_GROUP]
    db-[a:f].example.com
    ```
  - variables
    * host-specific variables (this includes connection port which can be
      specified either via colon at the host name or by variable ansible_port, see
      below)

    * group variables are applied to all hosts from the group, uses vars section:

    ```
    [HOST_GROUP:vars]
    foo=bar
    ```

    * there is inconsistency in variable type detection for INI format of
      inventory, so YAML is highly recommended

    * organizing them by creating directories:
      1. host and group variable files must use YAML syntax

      2. variable files loaded by searching paths relative to the inventory
	 file or the playbook file: for example, if inventory file is
	 `/etc/ansible/hosts`, host named `foobar` belonging to two groups
	 `bar` and `foo`, host will use variables from following file
	 locations:

      ```
      /etc/ansible/group_vars/bar  # optionally end in .yml, .yaml
      /etc/ansible/group_vars/foo
      /etc/ansible/host_vars/foobar
      ```

      3. if there are too many variables for single group_vars file, you can split
      them to the directory with additional layer of variables organization:

      ```
      /etc/ansible/group_vars/bar/db_settings
      /etc/ansible/group_vars/bar/cluster_settings
      ```

      4. there is inconsistency in behaviour between different ansible
	 commands: `ansible-playbook` can look for `group_vars/` and
	 `host_vars/` directories in the playbook directory, while others use
	 only the inventory directory. If you rely on `ansible-playbook`
	 behaviour, you must provide the `--playbook-dir` argument to the other
	 commands

      5. variables merging: [TODO][intro_inv_vars_merging]

      6. [behavioral parameters][intro_inv_connection_behaviour] (actually, variables)
  common variables are:
        - `ansible_connection`: possible values are `smart` (default), `ssh` or `paramiko`)
        - `ansible_host`: FQDN/IP address of managed host
        - `ansible_port`: self-explanatory
        - `ansible_user`: self-explanatory
        - `ansible_password`: self-explanatory
        - `ansible_ssh_private_key_file`: self-explanatory

  - inventory building:
    * static - described above, stored in the file

    this is a suitable option for on-premises infra or something really small
    in the cloud (like cloud VPSes but their owner does not use features like
    snapshotting, (auto)scaling and other fancy features

    * dynamic 

    this type of inventory relies on 3rd party API and does not provide any
    inventory files but script to gather them, see [link][intro_inv_dynamic]

  - using multiple sources (even combining each of two above):
    * [aggregating them][intro_inv_multiple_sources] with a directory


[intro_inv_vars_merging]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#how-variables-are-merged
[intro_inv_connection_behaviour]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#connecting-to-hosts-behavioral-inventory-parameters
[intro_inv_dynamic]: https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html
[intro_inv_multiple_sources]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#using-multiple-inventory-sources
